<?php
/**
 * Template name: Services
 */
get_header();
?>

			<section id="services" class="">

				<div class="wrapper limited">
					
					<?php 
					$counter = 0;

					$args=array(
						'post_type' => 'services',
						'post_status' => 'publish'
					);

					$services = new WP_Query($args);

					if($services->have_posts()) : ?>

					<?php while($services->have_posts()) : ?>
					<?php $services->the_post(); ?>
					<?php 
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
						$url = $thumb['0']; 
						$service_fields = get_post_meta( $post->ID, 'services', true ); 
					?>
						<section class="service services-height <?php echo ($counter % 2 == 1 ? 'leftside' : 'rightside'); echo ' post-'.$post->ID; ?>">
							<div id="<?php echo 'anchor_'.$post->ID; ?>" class="anchor"></div>
		

							<div class="photo services-height"  style="background-image: url(<?php echo $url; ?>);"></div>
							<div class="description services-height">
								<div class="table">
									<div class="cell middle spc">
										<div class="service-title">
											<h2><?php the_title();?></h2>
											<hr />
										</div>
										<div class="limiter">
											<?php the_content(); ?>
											
											<p><a class="show-content" href="#" rel="<?php echo 'post_'.$post->ID; ?>"><?php _e('Содержание услуги','nataly2015'); ?></a></p>
											<?php if ($service_fields[0]['projectfile']) : ?>
												<a class="button download" download href="<?php echo wp_get_attachment_url($service_fields[0]['projectfile']); ?>"><?php _e('Скачать пример проекта','nataly2015'); ?></a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
			
							<div id="<?php echo 'post_'.$post->ID; ?>" class="table-of-content services-height table">
								<?php echo $service_fields[0]['servicecontents']; ?>
							</div>
			
						</section>


					<?php $counter++; endwhile; ?>
					<?php endif; ?>
					


				</div>
			<div class="lookmore-holder">
				<a class="lookmore button arrow finished" href="<?php echo get_permalink( $page_contacts ); ?>">
					<span class="finish-label"><?php echo get_the_title( $page_contacts ); ?></span>
					<svg preserveAspectRatio="none" class="arrow-down" id="lookmore-arrow-down" viewBox="0 0 17 15">
					  <use xlink:href="#arrow"></use>
					</svg>
				</a>
		
			</div>
			</section>


<?php get_footer(); ?>