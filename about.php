<?php
/**
 * Template name: About
 */
get_header();
$aboutfields = get_post_meta( $post->ID, 'aboutfields', true );
$aboutslides = get_post_meta( $post->ID, 'features', true );
$videofields = get_post_meta( $post->ID, 'videofields', true );
$teamfields = get_post_meta( $post->ID, 'teamfields', true );
?>


<!-- block about -->
		<div id="about">

			<section id="homeabout" class="homeabout fullscreen">

				<div class="background-photo"></div>
				<div class="content Aligner">
					<div class="Aligner-item">
						<h2><?php echo $aboutfields[0]['title']; ?></h2>
						<p class="label"><?php echo $aboutfields[0]['subtitle']; ?></p>
						<hr />
						<div>
							<?php echo $aboutfields[0]['textcontent']; ?>
						</div>
					</div>					
				</div>
			
			</section>

			<div class="about-mobile-description">
				<h2><?php echo $aboutfields[0]['title']; ?></h2>
				<p class="label"><?php echo $aboutfields[0]['subtitle']; ?></p>
				<hr />
				<div>
					<?php echo $aboutfields[0]['textcontent']; ?>
				</div>
			</div>

<!-- block Team -->
		<?php if ($teamfields) { ?>
			<section id="team-holder" class="team-holder">
				<div class="title">
					<h3><?php _e('Наша команда','nataly2015'); ?></h3>
					<hr>
				</div>
	
				<div class="left prev browse">
					<svg preserveAspectRatio="none" class="slider-arrow-left" viewBox="0 0 17 15">
					  <use xlink:href="#arrow"></use>
					</svg>

				</div>
				<div class="right next browse">
					<svg preserveAspectRatio="none" class="slider-arrow-right" viewBox="0 0 17 15">
					  <use xlink:href="#arrow"></use>
					</svg>

				</div>
				
				<div class="swiper-container employee-swiper">
				
					<div class="swiper-wrapper">
					
					
					<?php foreach( $teamfields as $employee) { ?>
					<?php $photo = wp_get_attachment_image_src($employee['photo'], 'medium'); ?>
						<div class="employee-item swiper-slide">
							<div class="ava" style="background-image: url(<?php echo $photo[0]; ?>);"></div>
							<h3 class="name"><?php echo $employee['name']; ?></h3>
							<div class="rank"><?php echo $employee['position']; ?></div>
							<hr />
							<div class="desc">
								<?php echo $employee['description']; ?>
							</div>
						</div>
					<?php } ?>				

						
					</div>
					
				</div>

				<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/swiper.min.js"></script>

				<script type="text/javascript">

					var empSwiper = undefined;

					function employeeSwiper() {
						var screenWidth = window.innerWidth || $(window).width();
						if(screenWidth <= 1024) {
							$('.employee-swiper').removeClass('arrange-slides');
						    if (typeof empSwiper == 'undefined') {
						        empSwiper = new Swiper('.employee-swiper', {
						            slidesPerView: 5,
									spaceBetween: 20,
									loop: true,
									nextButton: '.slider-arrow-right',
									prevButton: '.slider-arrow-left',
									breakpoints: {
							            1024: {
							                slidesPerView: 3
							            },
							            768: {
							                slidesPerView: 2
							            },
							            639: {
							                slidesPerView: 1
							            }
							        }			
						        });
						    }
						} else {
							$('.employee-swiper').addClass('arrange-slides');
						    if (typeof empSwiper != 'undefined') {
						        empSwiper.destroy();
						        empSwiper = undefined;
						        $('.employee-swiper .swiper-wrapper').removeAttr('style');
						        $('.employee-swiper .swiper-slide').removeAttr('style');
						    }
						}

					}

					$(document).ready( function(){
						employeeSwiper();
						
					});
					$(window).on('resize', function(){
						employeeSwiper();
					});
				
				</script>
				
			</section>
		<?php } ?>
			
			<div class="lookmore-holder">
				<a class="lookmore button arrow finished" href="<?php echo get_permalink( $page_services ); ?>">
					<span class="finish-label"><?php echo get_the_title( $page_services ); ?></span>
					<svg preserveAspectRatio="none" class="arrow-down" id="lookmore-arrow-down" viewBox="0 0 17 15">
						<use xlink:href="#arrow"></use>
					</svg>
				</a>
			
			</div>
			
		</div>
	

<?php get_footer(); ?>
