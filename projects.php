<?php
/**
 * Template name: Projects
 */
get_header();
?>

			<section id="portfolio" class="">
				<div class="grey-bg">
					<div class="wrapper">
				
						<div id="portfolio_filters" class="filter-button-group filters button-group custom-select">
							

							
							<div class="title">
								<span id="title-text"><?php _e('Все','nataly2015'); ?></span>
								<svg preserveAspectRatio="none" class="" id="filters-arrow-down" viewBox="0 0 28 15">
								  <use xlink:href="#arrow-down"></use>
								</svg>
							</div>
							<div class="options">
								<button data-filter="*" class="is-checked"><?php _e('Все','nataly2015'); ?></button>
							<?php 
								$terms = get_terms("projectcategory"); 
								$count = count($terms);
								if ( $count > 0 ){
									foreach ( $terms as $term ) { ?>
										<button class="button" data-filter=".<?php echo $term->slug; ?>"><?php echo $term->name; ?></button>
									<?php }
								}
							?>
							</div>
						</div>
				
						<div id="projects_container" class="isotope_container">
							<!-- isotope plugin added here -->
				
							<!-- grid-sizer, gutter-sizer - are nesessary for isotope plugin-->
							<div class="grid-sizer"></div>
							<div class="gutter-sizer"></div>
						
						<?php 
						$paged = get_query_var('paged') ? get_query_var('paged') : 1;
						
						$args=array(
							'post_type' => 'projects',
							'post_status' => 'publish',
							'posts_per_page' => 36,
							'paged' => $paged
						);

						$projects = new WP_Query($args);

						if($projects->have_posts()) : ?>

							<?php while($projects->have_posts()) : ?>
							<?php $projects->the_post(); ?>
							<?php
								$projectfields = get_post_meta( $post->ID, 'projectfields', true );	
								$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large', false );
								$preview = wp_get_attachment_image_src( $projectfields[0]['gallerypreview'], 'full', false );
								$url = ($preview[0]) ? $preview[0] : $thumb['0']; 
								$current_cat = wp_get_post_terms( $post->ID, "projectcategory");
							?>

								<div class="item <?php echo $current_cat[0]->slug; ?>">
									<a href="<?php the_permalink(); ?>">
										<span class="project-img" style="background-image: url(<?php echo $url; ?>);"></span>
										<span class="project-text">
											<span class="title"><?php the_title(); ?></span>
											<span class="year"><?php echo $projectfields[0]['year']; ?></span>
										</span>
									</a>
								</div>
							
							<?php endwhile; ?>
							
						
						<?php else : ?>
							<p><?php _e('Ни один проект не добавлен.','nataly2015'); ?></p>
						<?php endif; ?>
							
						</div>
					</div>
				</div>
				<a class="lookmore button arrow" href="<?php echo get_permalink( $page_about ); ?>" id="moreProjects">
					<span class="primary-label"><?php _e('Посмотреть еще','nataly2015'); ?></span>
					<span class="finish-label"><?php echo get_the_title( $page_about ); ?></span>
					<svg preserveAspectRatio="none" class="arrow-down" id="lookmore-arrow-down" viewBox="0 0 17 15">
						<use xlink:href="#arrow"></use>
					</svg>
				</a>
				
				<div id="ajax_content_storage" style="display: none;"></div>
				
			</section>
			<?php
				wp_reset_query();
				global $post;
				$slug = get_post( $post )->post_name;
			?>
			
<script type="text/javascript">

	$(document).ready(function(){

		$.ajaxSetup({cache:false});
		var counter = 2;
		$(".lookmore").click(function(){
			
			if (!$(this).hasClass('finished') ) {
				$('#ajax_content_storage').load('<?php echo get_permalink( $page_portfolio ); ?>page/'+counter+' #projects_container .item', function(result) {
					var variable = $('#ajax_content_storage').html();
					if (variable) {
						$('#projects_container').append(variable).isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });
					} else {
						$('.lookmore').addClass('finished');
					}
				});
				counter++;
				return false;
			}
		});

	});		
	
</script>
<?php get_footer(); ?>
