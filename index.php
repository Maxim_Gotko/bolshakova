<?php get_header(); ?>

<section>
	<div class="wrapper limited">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


		 <h3 class="storytitle"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
	


		<?php 
		if ( has_post_thumbnail() ) { 
		  the_post_thumbnail('medium');
		} 
		?>		
		<?php the_content(__('(more...)')); ?>



		<?php endwhile; else: ?>
			<br />
			<br />
			<br />
			<br />
			<h1><?php _e('Ошибка 404','nataly2015'); ?></h1>
			<h3><?php _e('такой страницы не существует','nataly2015'); ?></h3>
		<?php endif; ?>
	</div>
</section>


<?php get_footer(); ?>
