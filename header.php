<?php handle_advertising_source(); 
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="<?php echo ($_SERVER['HTTP_HOST'] == 'nataly-bolshakova.kz') ? 'geo-kz' : 'geo-all'; ?> scrolling">

<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	
	<title><?php global $page, $post, $paged; wp_title( '', true, 'right' ); ?> <?php bloginfo('name'); ?></title>

	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/favicon.ico" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/vegas/vegas.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swipebox.css" />	
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swiper.min.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style-common.css" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/modernizr-1.7.min.js" async></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.2.min.js"></script>
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M97HKG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M97HKG');</script>
	<!-- End Google Tag Manager -->

	<script type="text/javascript">
		function h100PercentHack() {
			// var headerHeight = (window.innerWidth>1023)? 88:78;
			document.getElementById("fullscreen").innerHTML = '.fullscreen{height:' + (window.innerHeight) + 'px}';
		}
		// "100% height class" hack for class .fullscreen
		var el = document.createElement('style');
		el.id = 'fullscreen';
		el.type = 'text/css';
		document.getElementsByTagName('head')[0].appendChild(el);
		h100PercentHack();

		// var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
		var isMobile = navigator.userAgent.match( /(iPad)|(iPhone)|(iPod)|(Android)|(PlayBook)|(BB10)|(BlackBerry)|(Opera Mini)|(IEMobile)|(webOS)|(MeeGo)/i );
		if (isMobile) {
			window.addEventListener("orientationchange", function() {
				h100PercentHack();
			}, false);
		} else {
			window.onresize = h100PercentHack;
		}
	</script>
		
	<?php //получаем доп.поля из странциы контакты
	$contact_page_id = (ICL_LANGUAGE_CODE=='ru') ? 14 : 159;
	$recent = new WP_Query("page_id=".$contact_page_id); while($recent->have_posts()) : 
		$recent->the_post();
		$contactfields = get_post_meta( $post->ID, 'contactsfields', true );
	endwhile;
	wp_reset_query();
	$ph1link = str_replace(array(' ', '-', '(', ')'), '', get_phone()['phone_1']);
	$ph2link = str_replace(array(' ', '-', '(', ')'), '', get_phone()['phone_2']);
	
	?>		
		
	<!-- header is fixed -->
	<header>
		<div class="wrapper">
			<div class="logo">
				<a href="<?php bloginfo('url'); ?>" rel="home"></a>
			</div>	
			<nav id="menu">
				<ul>
					<?php wp_list_pages('exclude=2&title_li='); ?>
				</ul>
			</nav>
			<div class="phones">
				<ul>
					<li><span class="city"><?php (ICL_LANGUAGE_CODE=='ru') ? get_phone()['city_2'] : get_phone()['city_2en']; ?></span><!-- <a href="tel:<?php echo $ph2link; ?>" class="phone"><?php echo get_phone()['phone_2']; ?></a> --></li>
					<li><span class="city"><?php (ICL_LANGUAGE_CODE=='ru') ? get_phone()['city_1'] : get_phone()['city_1en']; ?></span><a href="tel:<?php echo $ph1link; ?>" class="phone show-while-scrolling"><?php echo get_phone()['phone_1']; ?></a></li>
				</ul>
			</div>
		
			<a class="toggle-menu menu-open">
				<span class="sandwich"></span>
			</a>
			
			
			
		</div>
		<?php //if( is_user_logged_in() ) { ?>
		<div class="lang-selector">
			<?php do_action('icl_language_selector'); ?>
		</div>
		<?php //} ?>	
	</header>	
	
	<div id="mob_menu">
		<nav class="side-menu">
			<a class="toggle-menu menu-close">
				<span class="cross">
					<svg preserveAspectRatio="none" class="close-menu" viewBox="0 0 17 15">
						<use xlink:href="#close-cross"></use>
					</svg>
				</span>
			</a>
			<ul>
				<?php wp_list_pages('exclude=2&title_li='); ?>	
				<?php
					//if( is_user_logged_in() ) {
						$languages = icl_get_languages();
						foreach($languages as $l){
							if ($l['active']==0) { ?>
								<li><a href="<?php echo $l['url']; ?>"><?php echo $l['native_name']; ?></a></li>
							<?php }
						}
					//}
				 ?>
			</ul>
			
			<div class="sidemenu-footer">
				<div class="phone">
					<a href="tel:<?php echo $ph1link; ?>"><?php echo get_phone()['phone_1']; ?></a>
				</div>

				<div class="address"><?php echo $contactfields[0]['address']; ?></div>

				<div class="social">
					<?php if($contactfields[0]['email']) : ?>
					<a href="mailto:<?php echo $contactfields[0]['email'];?>" class="soc-icon">
						<svg preserveAspectRatio="none" class="soc-icon-image mail" viewBox="0 0 32 32">
							<use xlink:href="#iconMenuMail"></use>
						</svg>
					</a>
					<?php endif; ?>
					<?php if($contactfields[0]['googleplus']) : ?>
					<a href="http://<?php echo $contactfields[0]['googleplus']; ?>" class="soc-icon" target="_blank">
						<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
							<use xlink:href="#iconGp"></use>
						</svg>
					</a>
					<?php endif; ?>
					<?php if($contactfields[0]['facebook']) : ?>
					<a href="http://<?php echo $contactfields[0]['facebook']; ?>" class="soc-icon" target="_blank">
						<svg preserveAspectRatio="none" class="soc-icon-image facebook" viewBox="0 0 32 32">
							<use xlink:href="#iconFb"></use>
						</svg>
					</a>
					<?php endif; ?>
					<?php if($contactfields[0]['youtube']) : ?>
					<a href="http://<?php echo $contactfields[0]['youtube']; ?>" class="soc-icon">
						<svg preserveAspectRatio="none" class="soc-icon-image youtube" viewBox="0 0 32 32" target="_blank">
							<use xlink:href="#iconYt"></use>
						</svg>
					</a>
					<?php endif; ?>
				</div>
				
			</div>
			
		</nav>
	</div>

	<div id="page">		

		<div id="pusher"><!-- pusher -->