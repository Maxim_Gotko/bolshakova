		</div> <!-- pusher -->	

		<footer>

			<div class="wrapper">
				
				<?php wp_reset_query();
				//получаем доп.поля из странциы контакты
				$contact_page_id = (ICL_LANGUAGE_CODE=='ru') ? 14 : 159;
				$recent = new WP_Query("page_id=".$contact_page_id); while($recent->have_posts()) : 
					$recent->the_post();
					$contactfields = get_post_meta( $post->ID, 'contactsfields', true );
				endwhile;
				$ph1link = str_replace(array(' ', '-', '(', ')'), '', get_phone()['phone_1']);
				?>
				
				<div class="news">
					<div class="for-contacts-page">
						<span><?php echo $contactfields[0]['footertext1']; ?></span>
						<span><?php echo $contactfields[0]['footertext2']; ?></span>
					</div>
					
					<ul class="news-list hide1023">
						<?php wp_reset_query();
						$args=array(
							'post_status' => 'publish',
							'posts_per_page' => 3,
						);
						$the_query = new WP_Query($args);
						if($the_query->have_posts()) : ?>
						<?php while($the_query->have_posts()) : ?>
						<?php $the_query->the_post(); ?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
						<?php endwhile; ?>
						<?php endif; ?>

					</ul>
				</div>
				<?php $page_contacts = icl_object_id(14, 'page', true); ?>
				<div class="contacts">
					<ul>
						<li><a href="<?php echo get_permalink( $page_contacts ); ?>">
							<svg preserveAspectRatio="none" class="footer-icon-pin" viewBox="0 0 5.03 7.512">
								<use xlink:href="#iconPin"></use>
							</svg><?php echo $contactfields[0]['address']; ?></a></li>
						<li><a href="mailto:<?php echo $contactfields[0]['email']; ?>">
							<svg preserveAspectRatio="none" class="footer-icon-mail" viewBox="0 0 7.834 5.193">
								<use xlink:href="#iconMail"></use>
							</svg><?php echo $contactfields[0]['email']; ?></a></li>
						<li class="copyr"><?php echo $contactfields[0]['copyright']; ?></li>
					</ul>
				</div>

				<div class="center">
					
					<div class="phone">
						
						<a href="tel:<?php echo $ph1link; ?>"><?php echo get_phone()['phone_1']; ?></a>
					</div>
					<div class="social">
					<?php if($contactfields[0]['facebook']) : ?>
						<a href="http://<?php echo $contactfields[0]['facebook']; ?>" class="soc-icon" target="_blank">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconFb"></use>
							</svg>
						</a>
					<?php endif; ?>
					<?php if($contactfields[0]['googleplus']) : ?>
						<a href="http://<?php echo $contactfields[0]['googleplus']; ?>" class="soc-icon" target="_blank">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconGp"></use>
							</svg>
						</a>
					<?php endif; ?>
					<?php if($contactfields[0]['pinterest']) : ?>
						<a href="http://<?php echo $contactfields[0]['pinterest']; ?>" class="soc-icon" target="_blank">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconPt"></use>
							</svg>
						</a>
					<?php endif; ?>
					<?php if($contactfields[0]['youtube']) : ?>
						<a href="http://<?php echo $contactfields[0]['youtube']; ?>" class="soc-icon" target="_blank">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconYt"></use>
							</svg>
						</a>
					<?php endif; ?>
					<?php if($contactfields[0]['instagram']) : ?>
						<a href="http://<?php echo $contactfields[0]['instagram']; ?>" class="soc-icon" target="_blank">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconInst"></use>
							</svg>
						</a>
					<?php endif; ?>
					</div>
					
				</div>
				
				<div class="copyr-mobile"><?php echo $contactfields[0]['copyright']; ?></div>
				

			</div>
		</footer>

	</div><!-- ///page -->	
	
<?php include (TEMPLATEPATH . '/svg-code.php'); ?>	

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/fontsmoothie.min.js" async></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/vegas/vegas.my.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.swipebox.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/functions-common.js"></script>

<?php wp_footer(); ?>
</body>
</html>