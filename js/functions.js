$(document).bind("scroll", function() {
	/* add '.scrolling' class to body while scrolling */
	toggleMenuOnSlide();
});


$(window).resize(function() {
	toggleMenuOnSlide();
});


$('#menu li a').mouseover(function() {
	$('#menu').addClass('hovered');
}).mouseout(function() {
	$('#menu').removeClass('hovered');
});

$('#shareProject .switcher').click( function(){
	$('#shareProject').toggleClass('active');
})



$(document).ready(function (){	

	// $('#pusher').bind('swipeone swiperight', switchMobMenu);
	toggleMenuOnSlide();
	
	//highlight active menu
	var pathArray = window.location.pathname.split( '/' );	
	$('#menu li a').each( function(){
		var curHref = $(this).attr('href');
		if( pathArray.slice(-1)[0] == curHref ) {
			$(this).addClass('active');
		}
	});
	
	$('body').addClass('loaded');
	
	/* Swipebox gallery (lightbox) */
	$( '.swipebox' ).swipebox({
		hideBarsDelay: false,
		removeBarsOnMobile: false
	});
	 
	$( function() {
		// init Isotope for portfolio page
		var $projects_container = $('#portfolio #projects_container');
		$projects_container.isotope({
			itemSelector: '.item',
			layoutMode: 'fitRows'
		});
		
		//init Isotope for project page
		var $project_container = $('#projectabout #container');
		$project_container.isotope({
			itemSelector: '#container .item',
			layoutMode: 'masonry',
			masonry: {
				columnWidth: "#container .grid-sizer",
				gutter: "#container .gutter-sizer",
				itemSelector: '#container .item'
			}

		});		
		//init Isotope for project page part II
		var $project_container = $('#extra_images #container2');
		$project_container.isotope({
			itemSelector: '#container2 .item',
			layoutMode: 'masonry',
			masonry: {
				columnWidth: "#container2 .grid-sizer",
				gutter: "#container2 .gutter-sizer",
				itemSelector: '#container2 .item'
			}

		});	

		// init Isotope for blog page
		var $blog_container = $('#blog #blog_container').imagesLoaded( function() {
			$blog_container.isotope({
				itemSelector: '.item',
				layoutMode: 'masonry',
				masonry: {
					columnWidth: "#blog_container .grid-sizer",
					gutter: "#blog_container .gutter-sizer",
					itemSelector: '#blog_container .item'
				}
			});
		});

		function getHashFilter() {
			// get filter=filterName
			var matches = location.hash.match( /filter=([^&]+)/i );
			var hashFilter = matches && matches[1];
			return hashFilter && decodeURIComponent( hashFilter );
		}

		$( function() {

			var $grid = $('.isotope_container');

			// bind filter button click
			var $filterButtonGroup = $('.filter-button-group');
			$filterButtonGroup.on( 'click', 'button', function() {
				var filterAttr = $( this ).attr('data-filter');
				// set filter in hash
				location.hash = 'filter=' + encodeURIComponent( filterAttr );
			});

			var isIsotopeInit = false;

			function onHashchange() {
				var hashFilter = getHashFilter();
				if ( !hashFilter && isIsotopeInit ) {
					return;
				}
				isIsotopeInit = true;
				// filter isotope
				$grid.isotope({
					itemSelector: '.item',
					layoutMode: 'masonry',
					filter: hashFilter
				});
				// set selected class on button
				if ( hashFilter ) {
					$filterButtonGroup.find('.is-checked').removeClass('is-checked');
					$filterButtonGroup.find('[data-filter="' + hashFilter + '"]').addClass('is-checked');
				}
			}

			$(window).on( 'hashchange', onHashchange );

			// trigger event handler to init Isotope
			onHashchange();

		});

	});
	 
});

// mobile slide menu 
( function( $ ) {
	$( '.toggle-menu' ).on( 'touchstart click', function(e) {
		switchMobMenu(e);
	} );
} )( jQuery );


function switchMobMenu(e) {
	e.preventDefault();

	var $body = $( 'body' ),
	$page = $( '#page' ),
	$menu = $( '#mob_menu' ),

	transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';

	$body.addClass( 'animating' );

	if ( $body.hasClass( 'menu-visible' ) ) {
		$body.addClass( 'right' );
	} else {
		$body.addClass( 'left' );
	}
	$page.on( transitionEnd, function() {
		$body
		.removeClass( 'animating left right' )
		.toggleClass( 'menu-visible' );

		$page.off( transitionEnd );
	});
}	
	
	
function toggleMenuOnSlide() {
	var docWidth = $(document).width();
	var scrollTop = $(document).scrollTop();

	if (scrollTop > 12) {
		$("header").addClass('sticky').removeClass('not-sticky');
	} else {
		$("header").removeClass('sticky').addClass('not-sticky');
	}

}


function animateMenu(stat, delay) {
	if(!delay) {
		delay = 0
		$('header, #pusher').removeClass('animated');
	} else {
		$('header, #pusher').addClass('animated');
	}
	if (stat == 'compact') {
		//показываем компактный вариант
		// $("body").removeClass('scrolling');
		// $(".menu-compact").fadeOut(delay);
		// $(".menu-expanded").fadeIn(delay);
		$("body").addClass('scrolling');
		$(".menu-expanded").fadeOut(delay);
		$(".menu-compact").fadeIn(delay);
	}
	if (stat == 'expand') {
		//показываем расширенный вариант
		// $("body").removeClass('scrolling');
		// $(".menu-compact").fadeOut(delay);
		// $(".menu-expanded").fadeIn(delay);
		$("body").addClass('scrolling');
		$(".menu-expanded").fadeOut(delay);
		$(".menu-compact").fadeIn(delay);
	}
}

//sharing
var Share = {
	vk: function() {
		var p = Share.arg(arguments);
		url  = 'http://vkontakte.ru/share.php?';
		if (p.url) 	url += 'url=' + p.url;
		if (p.title) 	url += '&title=' +p.title;
		if (p.text) 	url += '&description=' + p.text;
		if (p.image) 	url += '&image='	   + p.image;
		url += '&noparse=true';
		Share.popup(url);
	},
	fb: function() {
		var p = Share.arg(arguments);
		url  = 'http://www.facebook.com/sharer.php?s=100';
		if (p.title) 	url += '&p[title]='	 + p.title;
		if (p.text) 	url += '&p[summary]='+ p.text;
		if (p.url) 		url += '&p[url]=' + p.url;
		if (p.image) 	url += '&p[images][0]=' + p.image;
		
		// console.log(p.image);
		Share.popup(url);
	},
	tw: function() {
		var p = Share.arg(arguments);
		url  = 'http://twitter.com/share?';
		if (p.url) url += 'url='	  + p.url
		if (p.title) url += '&text='	  + p.title
		if (p.url) url += '&counturl=' + p.url
		Share.popup(url);
	},
	gp: function() {
		var p = Share.arg(arguments);
		url  = 'https://plus.google.com/share?';
		if (p.url) url += 'url='	  + p.url
		if (p.title) url += '&text='  + p.title
		if (p.url) url += '&counturl=' + p.url
		Share.popup(url);
	},
	pt: function() {
		var p = Share.arg(arguments);
		url  = 'https://pinterest.com/pin/create/button/?';
		if (p.url) 		url += '&url=' + p.url;
		if (p.image) 	url += '&media=' + p.image;
		if (p.title) 	url += '&description='	 + p.title;
		Share.popup(url);
	},

	arg: function (args) {
		// 0 - url
		// 1 - title
		// 2 - text
		// 3 - image
		return	{
			url : encodeURIComponent(args[0] || document.location.origin + document.location.pathname),
			title: encodeURIComponent(args[1] || ''),
			text: encodeURIComponent(args[2] || ''),
			image: encodeURIComponent(args[3] || '')
		}
	},
	popup: function(url) {
		// window.open(url,'','toolbar=0,status=0,width=626,height=436');
		window.open(url,'','toolbar=0,status=0,width=573,height=436');
	}
};

//events

// Listen for orientation changes


$(".scrolldown").click(function(event){
	event.preventDefault();
	dest = $(this.hash).offset().top;
	$('html,body').animate({scrollTop:dest-60}, 1000,'swing');
});

//services. show panels with description
$(".show-content").click(function(){
	var elem_id = $(this).attr('rel');
	var elem = $('#'+elem_id);
	elem.toggleClass('show');
	return false;	 
});

$('.custom-select .title').click( function(){
	$(this).siblings('.options').toggleClass('opened');
});
$('.custom-select .options button').click( function(){
	var optionValue = $(this).html();
	$(this).parent().parent().children('.title').children('span').html(optionValue);
	$(this).parent().removeClass('opened');
});

