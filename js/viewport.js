/*
 * Viewport - jQuery selectors for finding elements in viewport
 *
 * Copyright (c) 2008-2009 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *  http://www.appelsiini.net/projects/viewport
 *
 */
(function($) {

    $.topBelowTheScreenCenter = function(element, settings) {	
        var elementTop = $(element).offset().top - settings.threshold;
        var screenCenter = $(window).scrollTop() + $(window).height()*0.5;
        return screenCenter < elementTop;
    };
    $.btmAboveTheScreenCenter = function(element, settings) {	
        var screenCenter = $(window).scrollTop() + $(window).height()*0.5;
		var elementTop = $(element).offset().top + $(element).height() - settings.threshold;
        return screenCenter >= elementTop;
    };
	
    $.inviewport = function(element, settings) {
        return !$.topBelowTheScreenCenter(element, settings) && !$.btmAboveTheScreenCenter(element, settings);
    };
    
    $.extend($.expr[':'], {
		
        "topBelowTheScreenCenter": function(a, i, m) {
            return $.topBelowTheScreenCenter(a, {threshold : 0});
        },
        "btmAboveTheScreenCenter": function(a, i, m) {
            return $.btmAboveTheScreenCenter(a, {threshold : 0});
        },
        "in_viewport": function(a, i, m) {
            return $.inviewport(a, {threshold : 0});
        }
    });

    
})(jQuery);