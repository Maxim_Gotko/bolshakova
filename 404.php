<?php include "header.php"; ?>

	<section class="fullscreen404 error404-bg">
		<div class="content">
	
			<h2>404<span class="buble_404">ошибка</span></h2>
		
			<h3>Страница не существует</h3>
			<hr />
			<h4>Приносим свои извинения за неудобства.</h4>
			<p>Предлагаем Вам перейти:</p>

			<ul class="thumbnails">
				<li>
					<a href="<?php bloginfo('url'); ?>" title=""  class="black-hover"><img src="<?php bloginfo('template_url'); ?>/img/404_img1.jpg" alt="Главная"></a>
					<a href="<?php bloginfo('url'); ?>" class="button button-small">Главная</a>
				</li>
				<li>
					<a href="<?php echo get_permalink( $page_portfolio ); ?>" title="" class="black-hover"><img src="<?php bloginfo('template_url'); ?>/img/404_img2.jpg" alt="Портфолио"></a>
					<a href="<?php echo get_permalink( $page_portfolio ); ?>" class="button button-small">Портфолио</a>
				</li>
				<li>
					<a href="<?php echo get_permalink( $page_services ); ?>" title="" class="black-hover"><img src="<?php bloginfo('template_url'); ?>/img/404_img3.jpg" alt="Услуги"></a>
					<a href="<?php echo get_permalink( $page_services ); ?>" class="button button-small">Услуги</a>
				</li>
			</ul>
		</div>
	</section>


				
	
<?php include "footer.php"; ?>

		<script>		
				// "100% height class" hack
				function h100Percent404() {
					var footerHeight = (window.innerWidth > 639)? ((window.innerWidth > 1024)? 97 : 100) : 208 ;
					document.getElementById("fullscreen404").innerHTML = '.fullscreen404{min-height:' + (window.innerHeight - footerHeight) + 'px}';
				}
				var el = document.createElement('style');
				el.id = 'fullscreen404';
				el.type = 'text/css';
				document.getElementsByTagName('head')[0].appendChild(el);
				$(document).ready( function(){
					h100Percent404();
				});
				window.onresize = h100Percent404;
				
		</script>