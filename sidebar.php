<?php wp_reset_query(); ?>

<?php
	if ( ICL_LANGUAGE_CODE=='en' ) {
		$blogpage = 171;
	} else {
		$blogpage = 11;
	}
?>

<?php $contacts_page = new WP_Query("page_id=".$blogpage); while($contacts_page->have_posts()) : $contacts_page->the_post();?>
	<?php $blogfeatured = get_post_meta( $post->ID, 'blogfeatured', true ); ?>
	
		<?php if ($blogfeatured) : ?>
			<section class="sidebar">
				<ul class="news-list">
					<?php foreach ($blogfeatured as $item) : ?>
						<?php $thumb = wp_get_attachment_image_src( $item['image'], 'thumbnail' ); ?>
					<li class="news-item">
						<div class="img-container"><a class="black-hover" href="<?php echo $item['url']; ?>"><img src="<?php echo $thumb[0]; ?>" /></a></div>
						<div class="text-container">
							<h4><a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></h4>
							<p><?php echo $item['description']; ?></p>
						</div>
					</li>
					<?php endforeach; ?>
				</ul>
			</section>
	    <?php endif; ?>
	<?php ?>
<?php endwhile; ?>