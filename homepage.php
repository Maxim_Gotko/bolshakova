<?php
/**
 * Template name: Homepage
 */
get_header();
$homeslides = get_post_meta( $post->ID, 'homeslides', true );
$homeabout = get_post_meta( $post->ID, 'homeabout', true );
$homemosaic = get_post_meta( $post->ID, 'homemosaic', true );
// $mosaic = explode(',', $homemosaic[0]['ids_array']);
$mosaic = get_post_meta( $post->ID, 'projects_list', true );
?>


	<div id="homepage">
		<section id="homeslider" class="fullscreen">
			<div class="homeslider fullscreen">
				
				<a href="#" class="slider-next">
					<svg preserveAspectRatio="none" class="slider-arrow-right" viewBox="0 0 17 15">
						<use xlink:href="#arrow"></use>
					</svg>
				</a>
				
				<a href="#" class="slider-prev">
					<svg preserveAspectRatio="none" class="slider-arrow-left" viewBox="0 0 17 15">
						<use xlink:href="#arrow"></use>
					</svg>
				</a>
				
				<ul class="awards">
					<li>
						<a href="https://propertyawards.net/europe-2015/" title="European Property Awards - Winner" target="_blank" class="external" rel="nofollow"><img src="<?php bloginfo('template_url'); ?>/img/award1.png" alt="https://propertyawards.net/europe-2015/"></a>
					</li>
					<li>
						<a href="http://www.internationaldesignexcellenceawards.com/past-winners/2012-winners/" title="SBID 2012 Winner" target="_blank" class="external" rel="nofollow"><img src="<?php bloginfo('template_url'); ?>/img/award2.jpg" alt="http://www.internationaldesignexcellenceawards.com/past-winners/2012-winners/"></a>
					</li>
					<li>
						<a href="http://www.sbid.org/" title="SBID Professional" target="_blank" class="external" rel="nofollow"><img src="<?php bloginfo('template_url'); ?>/img/award3.png" alt="http://www.sbid.org/"></a>
					</li>
				</ul>
				
				<a href="#homeabout" class="scrolldown  hide639">
					<svg preserveAspectRatio="none" class="arrow-down" viewBox="0 0 17 15">
						<use xlink:href="#arrow"></use>
					</svg>
				</a>

				<div>
				<?php if ($homeslides) { ?>
					<?php foreach( $homeslides as $slide) { ?>
						<div class="slide_text <?php if ($slide['blacktext']) : ?>dark-text<?php endif; ?>">
							<div class="text-screen"></div>
							<h2 class="white"><?php echo $slide['title']; ?></h2>
							<hr class="white" />
							<p class="white"><?php echo $slide['description']; ?></p>
						</div>
					<?php } ?>
				<?php } ?>
				</div>

			</div>
		
		</section>
		<?php if ($homeabout) { ?>
		<div class="gray-separator"></div>
		<section id="homeabout" class="homeabout fullscreen">

			<div class="background-photo"></div>
			<div class="content Aligner">
				<div class="Aligner-item">
					<h2><?php echo $homeabout[0]['title']; ?></h2>
					<hr class="hr-1" />
					<p class="label"><?php echo $homeabout[0]['subtitle']; ?></p>
					<hr class="hr-2" />
					<div class="detailed">
						<?php echo $homeabout[0]['textcontent']; ?>
						<p class="rmore">
							<a href="<?php echo get_permalink( $page_about ); ?>" class="readmore">
								<svg preserveAspectRatio="none" class="arrow-right" viewBox="0 0 17 15">
									<use xlink:href="#arrow"></use>
								</svg>
							</a>
						</p>
					</div>
				</div>				
				
			</div>
			
		</section>
		<?php } ?>

		<section id="homeprojects" class="">
			<h2 class="show639 blocktitle"><?php _e('Проекты','nataly2015'); ?></h2>
			<div class="wrapper limited">
				<div id="container">
					
				<!-- grid-sizer, gutter-sizer - are nesessary for masonry plugin-->
					<div class="grid-sizer"></div>
					<div class="gutter-sizer"></div>
					
					
					

					
				</div>
			</div>

			
			<div id="ajax_content_storage" style="display: none;">
				
				<?php 
			
				$args=array(
					'post_type' => 'projects',
					'post_status' => 'publish',
					'posts_per_page' => 8,
					'post__in' => $mosaic,
					'orderby' => 'post__in',
				);

				$projects = new WP_Query($args);
				$i = 0;
				if($projects->have_posts()) : ?>
					<?php $iterat = 1; $ppage = 1; ?>
					<?php while($projects->have_posts()) : ?>
					<?php $i++; ?>
					<?php $projects->the_post(); ?>
					<?php
						if ($iterat == 1 || $iterat == 8) {
							$blockclass = 'w2';
						} else if ($iterat == 3 || $iterat == 4) {
							$blockclass = 'h2';
						} else {
							$blockclass = '';
						}
						$img_size = ($blockclass == 'h2') ? 'full' : 'large';
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($projects->ID), $img_size );
						$thumb_url = $thumb['0'];	
						$projectfields = get_post_meta( $post->ID, 'projectfields', true );
						$proj_home_img = wp_get_attachment_image_src($projectfields[0]['homeimage'], $img_size);
						$proj_home_img = ($proj_home_img) ? $proj_home_img : $thumb;
						$iterat = ($iterat == 9) ? 1 : $iterat;
					?>
					<?php if ($iterat == 1) { ?>
						<div class="page-<?php echo $ppage; ?>">
					<?php }?>
							<div class="item <?php echo $blockclass; ?>">
								<div class="image-holder" style="background-image: url(<?php echo $proj_home_img[0]; ?>);"></div>
								<?php if($show_proj_counter && is_user_logged_in() ) { ?>
									<div class="bl-counter"><?php echo $i; ?></div>
								<?php } ?>
								<div class="hover-bg table">
									<a href="<?php the_permalink(); ?>" class="hover-text">
										<span class="table">
											<span class="cell middle">
												<span class="h3"><?php the_title(); ?></span>
												<span class="year"><?php echo $projectfields[0]['year']; ?></span>
											</span>
										</span>
									</a>
								</div>
								<a class="mobile-arrow" href="<?php the_permalink(); ?>">
									<svg preserveAspectRatio="none" class="arrow-right" viewBox="0 0 17 15">
										<use xlink:href="#arrow"></use>
									</svg>
								</a>
							</div>
				
					<?php $iterat++; 
					if ($iterat == 9) { $ppage++; ?>
						</div>
					<?php } endwhile; ?>
				
			
				<?php endif; ?>

			</div>
		</section>
		
		<div class="lookmore-holder">
			<a class="lookmore button arrow" id="moreHomeProjects" href="<?php echo get_permalink( $page_portfolio ); ?>">
				<span class="primary-label"><?php _e('Посмотреть еще','nataly2015'); ?></span>
				<span class="finish-label"><?php echo get_the_title( $page_portfolio ); ?></span>
				<svg preserveAspectRatio="none" class="arrow-down" id="lookmore-arrow-down" viewBox="0 0 17 15">
				  <use xlink:href="#arrow"></use>
				</svg>
			</a>
		</div>
		

	</div>

<script type="text/javascript">
	


	$(document).ready(function(){

		$.ajaxSetup({cache:false});
		var variable = $('#ajax_content_storage .page-1').html();
		// console.log(variable);
		$('#homeprojects #container').append(variable);
		
		//init Isotope for home page
		var $homeprojects_container = $('#homeprojects #container');
		$homeprojects_container.isotope({
			itemSelector: '#homeprojects #container .item',
			layoutMode: 'masonry',
			masonry: {
				columnWidth: "#homeprojects #container .grid-sizer",
				gutter: "#homeprojects #container .gutter-sizer",
				itemSelector: '#homeprojects #container .item'
			}
		});
		
		var counter = 2;
		$(".lookmore").click(function(){
			
			if (!$(this).hasClass('finished') ) {

				variable = $('#ajax_content_storage .page-'+counter).html();
				if (variable) {
					$('#homeprojects #container').append(variable).isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });
				} else {
					$('.lookmore').addClass('finished');
				}
				counter++;
				return false;
			}
		});
		

	});		
	

	$( function() {
		$('.homeslider').vegas({
			delay: 7000,
			timer: false,
			slides: [
				<?php foreach( $homeslides as $slide) { ?>
				{ src: '<?php echo wp_get_attachment_url($slide['image']); ?>' },
				<?php } ?>
			],
			transitionDuration: '2000'
		});
	});
					
	$(document).ready(function() {
		
		$('.slider-next').click(function(e){
				e.preventDefault();
				$('.homeslider').vegas('next');
		});
		
		$('.slider-prev').click(function(e){
				e.preventDefault();
				$('.homeslider').vegas('previous');
		});


		$('body').bind('vegaswalk', function(e, bg, step) {
			
			$('.slide_text:eq(' + window.SwitchHomeCount + ')').stop(false, true).fadeOut(1000);
			
			SwitchHomeCount = bg;
			
			$('.slide_text:eq(' + window.SwitchHomeCount + ')').fadeIn(1000);

		});
	});

</script>
			


<?php get_footer(); ?>
