<?php get_header(); ?>

<?php 
	if (have_posts()) : while (have_posts()) : the_post(); 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$url = $thumb['0'];	
	$single_id = $post->ID;
	$category = get_the_category();
	$firstCategory = $category[0]->slug;
	$aligntop = get_post_meta( $post->ID, 'blogfields', true );
?>

	
			<section id="single-new" class="">
				<div class="wrapper limited">
					
					<div class="filters button-group custom-select">
						<div class="title">
							<span id="title-text"><?php _e('Новости','nataly2015'); ?></span>
							<svg preserveAspectRatio="none" class="" id="filters-arrow-down" viewBox="0 0 28 15">
							  <use xlink:href="#arrow-down"></use>
							</svg>
						</div>
						<div class="options">
							<button onclick="window.location.href='<?php echo get_permalink( $page_blog ); ?>#filter=*'"><?php _e('Все','nataly2015'); ?></button>
							<?php 
								$terms = get_terms("category"); 
								$count = count($terms);
								if ( $count > 0 ){
									foreach ( $terms as $term ) { ?>
							<button class="<?php if($firstCategory == $term->slug) { echo 'is-checked'; } ?>" onclick="window.location.href='<?php echo get_permalink( $page_blog ); echo "#filter=.".$term->slug; ?>'"><?php echo $term->name; ?></button>
									<?php }
								}
							?>
						</div>
					</div>
					
				</div>
				<div class="wrapper content-holder limited tablet-white-bg">
					<div class="columns">
						<section class="page_content">
							<?php if ($url) : ?>
							<div class="header">
								<div class="img-container big-img" style="background-image: url(<?php echo $url; ?>); background-position: 50% <?php echo ( count($aligntop[0]['thumbalign']) == 1 ) ? $aligntop[0]['thumbalign'] : '50%'; ?>;">

									<div class="social">
									
										<a onclick="Share.fb(); return false;" href="#" class="soc-icon">
											<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
												<use xlink:href="#iconFb"></use>
											</svg>
										</a>
										<a onclick="Share.gp(); return false;" href="#" class="soc-icon">
											<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
												<use xlink:href="#iconGp"></use>
											</svg>
										</a>
										<a onclick="Share.tw('<?php the_permalink(); ?>','<?php the_title(); ?>','','<?php echo $url; ?>'); return false;" href="#" class="soc-icon">
											<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
												<use xlink:href="#iconTw"></use>
											</svg>
										</a>
									<!--	<a onclick="Share.vk(); return false;" href="#" class="soc-icon">
											<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
												<use xlink:href="#iconVk"></use>
											</svg>
										</a>-->
										<a onclick="Share.pt('<?php the_permalink(); ?>','<?php the_title(); ?>','','<?php echo $url; ?>'); return false;" href="#" class="soc-icon">
											<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
												<use xlink:href="#iconPt"></use>
											</svg>
										</a>

									</div>
								</div>

							</div>
							<?php endif; ?>
							<div class="content">

								<h1><?php the_title(); ?></h1>
								<hr />
								<?php if ( has_excerpt($post->ID) ) : ?>
									<p class="label"><?php echo get_the_excerpt(); ?></p>
								<?php endif; ?>
								

								<?php the_content(); ?>
								<div class="clb"></div>
<script type="text/javascript">

	function twoImgHolder() {
		$(".double-image").height( $(".double-image").width()/2 );
	}

	$(document).ready( function() {
		$(".content a").each( function() {
			if ( $(this).has("img").length ) {
				var imgClass = $(this).children('img').attr('class');
				$(this).addClass('swipebox').attr('data-id', imgClass.replace(/\D/g, '')).attr('data-postname', '<?php the_title(); ?>');
			}
		});
		var iter = 1;
		$('.double-image img').each(function() {
			$(this).addClass('image'+iter);
			if ($(this).width() > $(this).height()) {
				$(this).addClass('landscape');        
			}
			iter++;
	    });

		twoImgHolder();

	});

	$(window).resize(function() {
		twoImgHolder();
	});
	
</script>

							</div>
							<div class="footer">

								<div class="social">

									<span class="text"><?php _e('Рассказать друзьям','nataly2015'); ?>:</span>

									<a onclick="Share.fb(); return false;" href="#" class="soc-icon">
										<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
											<use xlink:href="#iconFb"></use>
										</svg>
									</a>
									<a onclick="Share.gp(); return false;" href="#" class="soc-icon">
										<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
											<use xlink:href="#iconGp"></use>
										</svg>
									</a>
									<a onclick="Share.tw('<?php the_permalink(); ?>','<?php the_title(); ?>','','<?php echo $url; ?>'); return false;" href="#" class="soc-icon">
										<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
											<use xlink:href="#iconTw"></use>
										</svg>
									</a>
								<!--	<a onclick="Share.vk(); return false;" href="#" class="soc-icon">
										<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
											<use xlink:href="#iconVk"></use>
										</svg>
									</a>-->
									<a onclick="Share.pt('<?php the_permalink(); ?>','<?php the_title(); ?>','','<?php echo $url; ?>'); return false;" href="#" class="soc-icon">
										<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
											<use xlink:href="#iconPt"></use>
										</svg>
									</a>
								</div>
							</div>
							
						</section>
	<!-- sidebar  -->
						<?php get_sidebar();?>
								
	<!-- END: sidebar  -->



					</div>

					<div class="bottom-nav-buttons">
						
						
						<?php $prv_post = get_next_post(); //поменяли порядок
						$next_post = get_previous_post(); ?>
					

						<a class="arrow leftside <?php echo (!empty($prv_post)) ? '' : 'disabled';?>" href="<?php echo (!empty($prv_post)) ? get_permalink($prv_post->ID) : 'javascript:void(0)'; ?>" title="<?php echo (!empty($prv_post)) ? get_the_title($prv_post->ID) : ''; ?>">
							<span class="hide639"><?php _e('Предыдущая','nataly2015'); ?></span><span class="show639"><?php _e('Пред.','nataly2015'); ?></span> <?php _e('новость','nataly2015'); ?> <svg preserveAspectRatio="none" class="arrow-left" viewBox="0 0 17 15"><use xlink:href="#arrow"></use></svg>
						</a>

						<div class="button-holder">
							<a class="lookmore button" href="<?php echo get_permalink( $page_blog ); ?>">
								<?php _e('Все новости','nataly2015'); ?>
							</a>
						</div>

						<a class="arrow rightside <?php echo (!empty($next_post)) ? '' : 'disabled';?>" href="<?php echo (!empty($next_post)) ? get_permalink($next_post->ID) : 'javascript:void(0)'; ?>" title="<?php echo (!empty($next_post)) ? get_the_title($next_post->ID) : ''; ?>">
							<span class="hide639"><?php _e('Следующая','nataly2015'); ?></span><span class="show639"><?php _e('След.','nataly2015'); ?></span> <?php _e('новость','nataly2015'); ?> <svg preserveAspectRatio="none" class="arrow-right" viewBox="0 0 17 15"> <use xlink:href="#arrow"></use> </svg>
						</a>
					</div>
				</div>

			</section>
			
			


<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
