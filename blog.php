<?php
/**
 * Template name: Blog
 */
get_header();
?>

			<section id="blog" class="">
				<div class="grey-bg">
					<div class="wrapper">

						<div id="blog_filters" class="filter-button-group filters button-group custom-select">
							<div class="title">
								<span id="title-text"><?php _e('Все','nataly2015'); ?></span>
								<svg preserveAspectRatio="none" class="" id="filters-arrow-down" viewBox="0 0 28 15">
								  <use xlink:href="#arrow-down"></use>
								</svg>
							</div>
							<div class="options">
								<button data-filter="*" class="is-checked"><?php _e('Все','nataly2015'); ?></button>
					<?php 
						$terms = get_terms("category"); 
						$count = count($terms);
						if ( $count > 0 ){
							foreach ( $terms as $term ) { ?>
								<button class="button" data-filter=".<?php echo $term->slug; ?>"><?php echo $term->name; ?></button>
							<?php }
						}
					?>
							</div>
						</div>
				
						<div id="blog_container" class="isotope_container">
							<!-- isotope plugin added here -->
				
						<!-- grid-sizer, gutter-sizer - are nesessary for isotope plugin-->
						<div class="grid-sizer"></div>
						<div class="gutter-sizer"></div>
						
						<?php 
						$paged = get_query_var('paged') ? get_query_var('paged') : 1;
						$args=array(
							'post_status' => 'publish',
							'posts_per_page' => 50,
							'paged' => $paged
						);

						$the_query = new WP_Query($args);

						if($the_query->have_posts()) : ?>

							<?php while($the_query->have_posts()) : ?>
							<?php $the_query->the_post(); ?>
							<?php
								$blogfields = get_post_meta( $post->ID, 'blogfields', true ); 
								$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
								$thumb_spec = (isset($blogfields[0]['smallimage'])) ? wp_get_attachment_image_src($blogfields[0]['smallimage'], 'medium') : '';
								$url = ($thumb_spec[0]) ? $thumb_spec[0] : $thumb['0']; 
								$current_cat = wp_get_post_terms( $post->ID, 'category' );
							?>
					
								<div class="item <?php echo $current_cat[0]->slug; ?>">
									<?php if ($url) : ?>
									<a class="blog-img black-hover" href="<?php the_permalink(); ?>"><img class="horiz" src="<?php echo $url; ?>" /></a>
									<?php endif; ?>
									<div class="excerpt">
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<p><?php echo get_the_excerpt(); ?></p>
										<p class="date"><?php the_time('j.m.Y') ?></p>
									</div>
								</div>
							<?php endwhile; ?>
						
						<?php else : ?>
							<p><?php _e('Ни одной записи в блоге','nataly2015'); ?></p>
						<?php endif; ?>
						</div>
					</div>
				</div>
				<a class="lookmore button arrow" href="<?php echo get_permalink( $page_portfolio ); ?>" id="moreBlog">
					<span class="primary-label"><?php _e('Посмотреть еще','nataly2015'); ?></span>
					<span class="finish-label"><?php echo get_the_title( $page_portfolio ); ?></span>
					<svg preserveAspectRatio="none" class="arrow-down" id="lookmore-arrow-down" viewBox="0 0 17 15">
						<use xlink:href="#arrow"></use>
					</svg>
				</a>
				
				<div id="ajax_content_storage" style="display: none;"></div>
				
			</section>
			
			<?php
			wp_reset_query();
			global $post;
			$slug = get_post( $post )->post_name;
			?>
			
<script type="text/javascript">

	$(document).ready(function(){

		$.ajaxSetup({cache:false});
		var counter = 2;
		$(".lookmore").click(function(){
			
			if (!$(this).hasClass('finished') ) {
				$('#ajax_content_storage').load('<?php echo get_permalink( $page_blog ); ?>page/'+counter+' #blog_container .item', function(result) {
					var variable = $('#ajax_content_storage').html();
					if (variable) {
						$('#blog_container').append(variable);
						setTimeout( function(){
							$('#blog_container').isotope( 'reloadItems' ).isotope({ layoutMode: 'masonry' });
						}, 700);
					} else {
						$('.lookmore').addClass('finished');
					}
				});
				counter++;
				return false;
			}
		});

	});		
	
</script>
<?php get_footer(); ?>
