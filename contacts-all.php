<?php
/**
 * Template name: Contacts
 */
get_header();
$contactfields = get_post_meta( $post->ID, 'contactsfields', true );
$ph1link = str_replace(array(' ', '-', '(', ')'), '', get_phone()['phone_1']);
?>
			<section id="contacts">
				

					<div class="contact-info" id="collapseBlock">
						<div>
							<h4><?php echo $contactfields[0]['subtitle1']; ?></h4>
							<h3><?php echo $contactfields[0]['subtitle2']; ?></h3>
							<hr />
						</div>
						<div class="address"><?php echo $contactfields[0]['address']; ?></div>
						
						<div class="contact__column">
							<div class="contact__text"><?php echo $contactfields[0]['subparag1']; ?></div>
							<div class="phone"><a href="tel:<?php echo $ph1link; ?>"><?php echo get_phone()['phone_1']; ?></a></div>
							<div class="email"><a href="mailto:<?php echo $contactfields[0]['email']; ?>"><?php echo $contactfields[0]['email']; ?></a></div>
						</div>
						<div class="contact__column">
							<div class="contact__text"><?php echo $contactfields[0]['subparag2']; ?></div>
							<div class="phone"><a href="tel:<?php echo $ph1link; ?>"><?php echo get_phone()['phone_2']; ?></a></div>
							<div class="email"><a href="mailto:<?php echo $contactfields[0]['email_2']; ?>"><?php echo $contactfields[0]['email_2']; ?></a></div>
						</div>
					</div>					

				
				<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq2OBtXRmrPc6v_nTRcZBzAeoNngg61-I&sensor=false"></script>
				<div id="map-canvas" class="fullscreenmap"></div>
				
				
			</section>		

<?php get_footer(); ?>

				<script>
				
				var map;
				function gmaps_initialize(shiftX, shiftY) {
					var myLat = <?php echo $contactfields[0]['choordx']; ?>;
					var myLng = <?php echo $contactfields[0]['choordy']; ?>;
					shiftX = shiftX ? shiftX : 0;
					shiftY = shiftY ? shiftY : 0;

					var myLatLng = new google.maps.LatLng(myLat, myLng);
					var markerLatLng = new google.maps.LatLng(myLat-shiftY, myLng-shiftX);
					
					var mapOptions;
					
					if (isMobile) {
						mapOptions = {
			    			zoom: 17,
			    			center: markerLatLng,
							mapTypeControl: false,
							draggable: window.innerWidth < 676 || false,
							zoomControl: false,
							scrollwheel: false, 
							disableDoubleClickZoom: true
			  			}
					} else {
						mapOptions = {
			    			zoom: <?php echo $contactfields[0]['zoom']; ?>,
			    			center: markerLatLng,
							mapTypeControl: false,
			  			}
					}
						
						
		  			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

					var marker = new google.maps.Marker({
		      			position: myLatLng,
		      			map: map,
		      			icon: '<?php echo wp_get_attachment_url($contactfields[0]['marker']); ?>',
		      			title: '<?php echo $contactfields[0]['subtitle1']; echo ' '.$contactfields[0]['subtitle2']; ?>'
		  			});

		  			var infowindow = new google.maps.InfoWindow({
		        		content: '<div class="map-info"><?php echo $contactfields[0]['markerinfo']; ?></div>'
		      		});
		      		google.maps.event.addListener(marker, 'click', function() {   
		        		infowindow.open(map, marker);
		      		});
				}
				$(window).load( function() {
					var curW = $(document).width();
					if (curW < 1024) {
						gmaps_initialize('0','0.0005');
					} else if(curW > 1290){
						gmaps_initialize();
					} else {
						gmaps_initialize('-0.002');
					}
				});
				
				$(window).on('resize', function(){
					var curW = $(document).width();
					if (curW < 1024) {
						gmaps_initialize('0','0.0005');
					} else if(curW > 1290){
						gmaps_initialize();
					} else {
						gmaps_initialize('-0.002');
					}
				});
				
				
				// "100% height class" hack
				function h100PercentMap() {
					var pusherShift = 0;
					// if (window.innerWidth > 1279 ) {
					// 	pusherShift = 28;
					// } else {
					// 	pusherShift = 0;
					// }
					// var headerHeight = (window.innerWidth > 1279)? 88 : 55;
					var headerHeight = (window.innerWidth > 639)? 72 : 55;
					// var footerHeight = 70;
					var footerHeight = (window.innerWidth > 639)? 70 : 0;
					document.getElementById("fullscreenmap").innerHTML = '.fullscreenmap{height:' + (window.innerHeight - headerHeight - footerHeight + pusherShift) + 'px}';
				}
				var el = document.createElement('style');
				el.id = 'fullscreenmap';
				el.type = 'text/css';
				document.getElementsByTagName('head')[0].appendChild(el);
				$(document).ready( function(){
					h100PercentMap();
				});
				window.onresize = h100PercentMap;

				$("#collapseBlock").on('click', function(e) {
					if (isMobile && window.innerWidth < 639 && e.target.tagName != 'A') {
						$(this).toggleClass('collapsed');
					}
				});
				
			    </script>


