<?php

	$show_proj_counter = get_field('shownumber', 'options');

	$page_services = icl_object_id(9, 'page', true);
	$page_blog = icl_object_id(11, 'page', true); 
	$page_portfolio = icl_object_id(5, 'page', true);
	$page_contacts = icl_object_id(14, 'page', true);
	$page_about = icl_object_id(7, 'page', true);
	
	add_theme_support( 'post-thumbnails' );

	show_admin_bar(false);
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	
	if( function_exists('acf_add_options_page') ) {
		$page = acf_add_options_page(array(
			'page_title' 	=> 'My Options Page',
			'menu_title' 	=> 'My Options Page',
			'menu_slug' 	=> 'options-settings',
			'capability' 	=> 'edit_posts',
			'redirect' 	=> false
		));
	}
	
	if( function_exists('acf_add_options_sub_page') ){
		acf_add_options_sub_page(array(
			'title' => 'Din aftale',
			'parent' => 'acf-options'
		));
	}
	
// выводить ID в списке постов в адимнке
	
	function true_id($args){
		$args['post_page_id'] = 'ID';
		return $args;
	}
	function true_custom($column, $id){
		if($column === 'post_page_id'){
			echo $id;
		}
	}
	add_filter('manage_pages_columns', 'true_id', 5);
	add_action('manage_pages_custom_column', 'true_custom', 5, 2);
	add_filter('manage_posts_columns', 'true_id', 5);
	add_action('manage_posts_custom_column', 'true_custom', 5, 2);

	function excerpt($limit, $dotted) {
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		if (count($excerpt)>=$limit) {
			array_pop($excerpt);
			if ($dotted == true) {
				$excerpt = implode(" ",$excerpt).'...';
			} else {
				$excerpt = implode(" ",$excerpt);
			}
			
		} else {
			$excerpt = implode(" ",$excerpt);
		}	
		$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
		return $excerpt;
	}
 


// оформляем стандартную галлерею wordpress под vegas slider 
add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
	global $post;

	if (isset($attr['orderby'])) {
		$attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
		if (!$attr['orderby'])
			unset($attr['orderby']);
	}

	extract(shortcode_atts(array(
		'order' => 'ASC',
		'orderby' => 'menu_order ID',
		'id' => $post->ID,
		'itemtag' => 'dl',
		'icontag' => 'dt',
		'captiontag' => 'dd',
		'columns' => 3,
		'size' => 'thumbnail',
		'include' => '',
		'exclude' => ''
	), $attr));

	$id = intval($id);
	$galid = rand();
	if ('RAND' == $order) $orderby = 'none';

	if (!empty($include)) {
		$include = preg_replace('/[^0-9,]+/', '', $include);
		$_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

		$attachments = array();
		foreach ($_attachments as $key => $val) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	}

	if (empty($attachments)) return '';

	$output = "<div class=\"slider-box fullwidth\"><div id=\"slider_$galid\" class=\"newsslider\"><a href=\"#\" class=\"slider-next\"><svg preserveAspectRatio=\"none\" class=\"slider-arrow-right\" viewBox=\"0 0 17 15\"><use xlink:href=\"#arrow\"></use></svg></a><a href=\"#\" class=\"slider-prev\"><svg preserveAspectRatio=\"none\" class=\"slider-arrow-left\" viewBox=\"0 0 17 15\"><use xlink:href=\"#arrow\"></use></svg></a><span class=\"slide-info\"></span></div></div>\n";
	$output .= "<script type=\"text/javascript\"> $( function() { $(\"#slider_$galid\").vegas({ delay: 7000, timer: false, slides: [";

		// Now you loop through each attachment
		foreach ($attachments as $id => $attachment) {
			$img = wp_get_attachment_image_src($id, 'full');
			
			list($im_width, $im_height) = getimagesize($img[0]);
			$cover = ($im_height > $im_width) ? 'false' : 'true';

			$output .= "{src: \"{$img[0]}\", cover: ".$cover."},";

		}

		$output .= "], walk: function (index, slideSettings) { var total = this[0][\"_vegas\"][\"total\"]; $(\"#slider_$galid .slide-info\").text ( (index+1) + \"/\" + total); }, transitionDuration: \"2000\" }); }); $(document).ready(function() { $(\"#slider_$galid .slider-next\").click(function(e){ e.preventDefault(); $(\"#slider_$galid\").vegas(\"next\"); }); $(\"#slider_$galid .slider-prev\").click(function(e){ e.preventDefault(); $(\"#slider_$galid\").vegas(\"previous\"); }); }); </script>\n";

		return $output;
		$id = '';
	}


	
	// izwerg
	function get_advertising_parameter()
	{
		$advid = '';
		if(isset($_GET["advid"]))
		{
			$advid = substr(trim($_GET["advid"]), 0, 32);
		}
		if($advid == '')
		{
			return isset($_COOKIE['advid']) ? $_COOKIE['advid'] : '';
		}

		return $advid;
	}

	function handle_advertising_source()
	{
		if(!isset($_GET["advid"])) return;

		$advid = strtolower(substr(trim($_GET["advid"]), 0, 32));
		if($advid != '')
		{
			setcookie("advid", $advid, 0, '/');
		}
	}

	function get_phone()
	{
		$advid = get_advertising_parameter();
		$phone = [];

		if($advid != '') {
			$phone_list = get_field('phones-list', 'options');
			if($phone_list) {
				foreach ($phone_list as $ph) {
					if(strtolower($ph['adv-id']) == $advid) {
						$phone['phone_1'] = $ph['adv-id'] = $ph['phone'];
						$phone['city_1'] = $phone['city_1en'] = $phone['phone2'] = $phone['city_2'] = $phone['city_2en'] = '';
					}
				}
			}
		}

		if ($phone == '') {
			$phone['phone_1'] = $phone['city_1'] = $phone['city_1en'] = $phone['phone2'] = $phone['city_2'] = $phone['city_2en'] = '';

			$phone_loop = get_field('phonesloop', 'options');
			if($phone_loop) {
				foreach ($phone_loop as $phl) {
					if(strtolower($phl['domain']) == $_SERVER['HTTP_HOST']) {
						$phone['phone_1'] = $phl['phone_1'];
						$phone['city_1'] = $phl['city_1'];
						$phone['city_1en'] = $phl['city_1en'];
						$phone['phone_2'] = $phl['phone_2'];
						$phone['city_2'] = $phl['city_2'];
						$phone['city_2en'] = $phl['city_2en'];
					}
				}
			}
		}

		return $phone;
	}

	// Remove auto generated feed links
	function my_remove_feeds() {
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		remove_action( 'wp_head', 'feed_links', 2 );
	}
	add_action( 'after_setup_theme', 'my_remove_feeds' );
	
	// сортировка рубрик
	function my_category_order($orderby, $args)
	{
		if($args['orderby'] == 'sort')
			return 't.sort';
		else
			return $orderby;
	}

	add_filter('get_terms_orderby', 'my_category_order', 10, 2);
?>
