<?php 
	get_header();
	$projectfields = get_post_meta( $post->ID, 'projectfields', true );
	$blockconstuctor = get_post_meta( $post->ID, 'blockconstuctor', true );
	$projectgallery = get_post_meta( $post->ID, 'projectgallery', true );
	$clientcomment = get_post_meta( $post->ID, 'clientcomment', true );
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$url = $thumb['0'];
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	

			<section id="single-project" class="">

				<section id="project-photo" class="fullscreen" style="background-image: url(<?php echo $url; ?>);">
	
					<div class="text">

						<h1><?php the_title(); ?></h1>
						<hr class="centered" />
						<p><?php echo $projectfields[0]['subtitle']; ?></p>
					
					</div>

					<a href="#projectabout #container" class="scrolldown hide639">

						<svg preserveAspectRatio="none" class="arrow-down" viewBox="0 0 17 15">
						  <use xlink:href="#arrow"></use>
						</svg>

					</a>
					
					<div class="share" id="shareProject">
						<span class="switcher">
							<svg preserveAspectRatio="none" class="share-icon" viewBox="0 0 17 15">
								<use xlink:href="#share"></use>
							</svg>
						</span>
						<a onclick="Share.fb(); return false;" href="#" class="soc-icon fb">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconFb"></use>
							</svg>
						</a>
						<a onclick="Share.gp(); return false;" href="#" class="soc-icon gp">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconGp"></use>
							</svg>
						</a>
						<a onclick="Share.tw('<?php the_permalink(); ?>','<?php the_title(); ?>','','<?php echo $url; ?>'); return false;" href="#" class="soc-icon tw">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconTw"></use>
							</svg>
						</a>
					<!--	<a onclick="Share.vk(); return false;" href="#" class="soc-icon vk">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconVk"></use>
							</svg>
						</a>-->
						<a onclick="Share.pt('<?php the_permalink(); ?>','<?php the_title(); ?>','','<?php echo $url; ?>'); return false;" href="#" class="soc-icon pt">
							<svg preserveAspectRatio="none" class="soc-icon-image" viewBox="0 0 32 32">
								<use xlink:href="#iconPt"></use>
							</svg>
						</a>
					</div>
					
	
				</section>

		<?php if ($blockconstuctor) { ?>
				<section id="projectabout" class="">
					<div class="wrapper limited">
						<div id="container">
						<!-- grid-sizer, gutter-sizer - are nesessary for masonry plugin-->
							<div class="grid-sizer"></div>
							<div class="gutter-sizer"></div>
							<?php $i = 0; ?>
							<?php foreach( $blockconstuctor as $block) { ?>
							<?php
								$i++;
								$img_url = wp_get_attachment_url($block['blockimage']);
								$imgsize;
								if ($block['blocktype'] == 'narrow') {
									$blockclass = 'h2';
									$imgsize = 'large';
									$text_limit = 255;
								} else if ($block['blocktype'] == 'wide') {
									$blockclass = 'w2';
									$imgsize = 'large';
									$text_limit = ($block['avatar']) ? 480 : 530;
								} else if ($block['blocktype'] == 'wider') {
									$blockclass = 'w3';
									$imgsize = 'full';
									$text_limit = ($block['avatar']) ? 530 : 900;
								} else {
									$blockclass = '';
									$imgsize = 'large';
									$text_limit = 255;
								}

								//обрезаем текст что бы он не выходил за пределы блока
								$blocktext = iconv('UTF-8','windows-1251//TRANSLIT',$block['blocktext'] );
								$blocktext = substr($blocktext, 0, $text_limit);
								$blocktext = iconv('windows-1251','UTF-8',$blocktext );
								// $blocktext = str_replace('^2','²',$blocktext );
																
								$img_url_cutted = wp_get_attachment_image_src($block['blockimage'], $imgsize);
								$preview_url_cutted = wp_get_attachment_image_src($block['blockpreview'], $imgsize);
								$boximage = ($block['blockpreview']) ? $preview_url_cutted[0] : $img_url_cutted[0];
								
								$align_top = $block['aligntop'];
								$align_left = $block['alignleft'];
								
								($align_top) ? $align_top : '50%';
								($align_left) ? $align_left : '50%';
								
							?>
								<div class="item <?php echo $blockclass; echo (!$img_url) ? ' text' : ''; if($block['avatar']) { echo ' author-comment';} if($block['mobilecube'] == 'yes') {echo ' mobcube';} ?>" <?php echo ($boximage) ? 'style="background-position:'.$align_left.' '.$align_top.'; background-position-x: '.$align_left.'; background-position-y: '.$align_top.'; background-image: url('.$boximage.');"' : ''; ?>>

									<div class="table <?php echo ($block['blocktype'] == 'wider') ? 'wider-text' : ''; ?>">
										<?php if ($img_url) {  ?>
											<a href="<?php echo $img_url; ?>" class="swipebox link-block" data-id="<?= $block['blockimage']; ?>" data-postname="<?php the_title(); ?>" title="<?php echo $block['imagecaption']; ?>"></a>
										<?php } else { ?>
											<div class="service-text cell middle">
											<?php if ($block['avatar']) { ?>
												<img class="avatar" src="<?php echo wp_get_attachment_url($block['avatar']); ?>" />
												<div class="comment">
											<?php } ?>
													<h2><?php echo $block['title']; ?></h2>
													<?php if($block['subtitle']) : ?><p class="label subtitle"><?php echo $block['subtitle']; ?></p><?php endif; ?>
													<hr class="centered" />
													<p><?php echo $blocktext; ?></p>
											<?php if ($block['avatar']) { ?>
												</div>
											<?php } ?>
											</div>
										<?php } ?>
									</div>
								</div>
								
							<?php } ?>
			
					</div>
				</section>
			<?php } ?>
			<?php if ($projectgallery) { ?>
				
				<section id="extra_images">
					
					<div class="wrapper limited">
						<div id="container2">
						<!-- grid-sizer, gutter-sizer - are nesessary for masonry plugin-->
							<div class="grid-sizer"></div>
							<div class="gutter-sizer"></div>
						<?php foreach( $projectgallery as $slide) { ?>	
						<?php $i++; ?>
							<div class="item" style="background-image: url(<?php echo wp_get_attachment_url($slide['image']); ?>);">
								<?php if($show_proj_counter) { ?>
									<div class="bl-counter"><?php echo $i; ?></div>
								<?php } ?>
								<a href="<?php echo wp_get_attachment_url($slide['image']); ?>" class="swipebox" data-id="<?= $block['blockimage']; ?>" data-postname="<?php the_title(); ?>" title="<?php echo $slide['imagecaption']?>"></a>
							</div>
						<?php } ?>
						
						</div>
					</div>
				</section>	
				
			<?php } ?>
			<?php if ($clientcomment[0]['name']) { ?>	
				<div class="customer-comment">
					<div class="table">
						<div class="cell middle">
								<img class="avatar" src="<?php echo wp_get_attachment_url($clientcomment[0]['avatar']); ?>" />
								<div class="comment">
									<h2><?php echo $clientcomment[0]['name']?></h2>
									<?php if($clientcomment[0]['subtitle']) : ?><p class="label subtitle"><?php echo $clientcomment[0]['subtitle']?></p><?php endif; ?>
									<hr />
									<?php echo $clientcomment[0]['comment']?>
								</div>
						</div>
					</div>
				</section>
			<?php } ?>

				<section class="bottom-nav-buttons">

					<?php $prv_post = get_next_post(); //поменяли порядок
					$next_post = get_previous_post(); ?>
					

					<a class="arrow leftside <?php echo (!empty($prv_post)) ? '' : 'disabled';?>" href="<?php echo (!empty($prv_post)) ? get_permalink($prv_post->ID) : 'javascript:void(0)'; ?>" title="<?php echo (!empty($prv_post)) ? get_the_title($prv_post->ID) : ''; ?>">
						<span class="hide639"><?php _e('Предыдущий','nataly2015'); ?></span><span class="show639"><?php _e('Пред.','nataly2015'); ?></span> <?php _e('проект','nataly2015'); ?> <svg preserveAspectRatio="none" class="arrow-left" viewBox="0 0 17 15"><use xlink:href="#arrow"></use></svg>
					</a>

					<div class="button-holder">
						<a class="lookmore button" href="<?php echo get_permalink( $page_portfolio ); ?>">
							<?php _e('Все проекты','nataly2015'); ?>
						</a>
					</div>

					<a class="arrow rightside <?php echo (!empty($next_post)) ? '' : 'disabled';?>" href="<?php echo (!empty($next_post)) ? get_permalink($next_post->ID) : 'javascript:void(0)'; ?>" title="<?php echo (!empty($next_post)) ? get_the_title($next_post->ID) : ''; ?>">
						<span class="hide639"><?php _e('Следующий','nataly2015'); ?></span><span class="show639"><?php _e('След.','nataly2015'); ?></span> <?php _e('проект','nataly2015'); ?> <svg preserveAspectRatio="none" class="arrow-right" viewBox="0 0 17 15"> <use xlink:href="#arrow"></use> </svg>
					</a>
					
				</section>

			</section>
				

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>



<script type="text/javascript">
	// addition for swipebox, closing img on click on bg
	$(function(){
		$(document.body)
		.on('click touchend','#swipebox-slider .current img', function(e){
			return false;
		})
		.on('click touchend','#swipebox-slider .current', function(e){
			$('#swipebox-close').trigger('click');
		});
	});
</script>

<?php get_footer(); ?>
